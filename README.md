# margay

Margay is a simple web framework for Node.js.

## Documentation
* [margay](#export)
  * [margay.createServer([options])](#margay-createserver)
  * [margay.static(filePath[, statusCode[, mimeType]])](#margay-static)
* [Class: Response](#response)
  * [response.end([chunk])](#response-end)
  * [response.header(name, value)](#response-header)
  * [response.status(statusCode)](#response-status)
  * [response.write(chunk)](#response-write)
* [Class: Server](#server)
  * [server.default(functionOrPath)](#server-default)
  * [server.listen([port])](#server-listen)
  * [server.route(filter, functionOrPath)](#server-route)
  * [server.use(middleware)](#server-use)

### <a name="export"></a>margay

The object exported by the module.

#### <a name="margay-createserver"></a>margay.createServer()
* Returns: [\<Server\>](#server)

Returns an instance of the [Server](#server) class.

#### <a name="margay-static"></a>margay.static(filePath[, statusCode[, mimeType]])
* `filePath` [\<string\>](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type) Relative path to the file.
* `statusCode` [\<number\>](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#Number_type) Status code for the HTTP response. **Default:** `200`
* `mimeType` [\<string\>](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type) MIME type of the file.
* Returns: [\<Function\>](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function)

Returns a simple routing function that serves a static file.
If no MIME type is specified, the [mime](https://github.com/broofa/node-mime) module is used to detect the file's MIME type.

### <a name="response"></a>Class: Response

Represents an HTTP response from the server.

#### <a name="response-end"></a>response.end([chunk])
* `chunk` [\<Buffer\>](https://nodejs.org/api/buffer.html#buffer_class_buffer) | [\<string\>](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type) Optional data to write to the response stream right before it is closed.

Closes the response stream.

#### <a name="response-header"></a>response.header(name, value)
* `name` [\<string\>](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)
* `value` [\<any\>](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#Data_types)
* Returns: [\<this\>](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/this)

Sets a header of the HTTP response.

#### <a name="response-status"></a>response.status(statusCode)
* `statusCode` [\<number\>](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#Number_type)
* Returns: [\<this\>](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/this)

Sets the status code of the HTTP response.

#### <a name="response-write"></a>response.write(chunk)
* `chunk` [\<Buffer\>](https://nodejs.org/api/buffer.html#buffer_class_buffer) | [\<string\>](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type) Data to write to the response stream.
* Returns: [\<this\>](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/this)

Writes data to the response stream. Note that the data might be manipulated by middleware if you are using any.

### <a name="server"></a>Class: Server

Represents the HTTP server that routes requests.

#### <a name="server-default"></a>server.default(functionOrPath)
* `functionOrPath` [\<Function\>](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function) | [\<string\>](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type) Routing function or path to a static file.
* Returns: [\<Server\>](#server) A new server that will route unrouted requests to the specified function.

Sets the default action for when the server can't route a request.
When using a string, [margay.static(filePath[, statusCode[, mimeType]])](#margay-static) is implicitly called with a `statusCode` of `404`.

#### <a name="server-listen"></a>server.listen([port])
* `port` [\<number\>](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#Number_type) Port the server should listen on. **Default:** `80`
* Returns: [\<this\>](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/this)

Listens for HTTP requests on the specified port.

#### <a name="server-route"></a>server.route(filter, functionOrPath)
* `filter` [\<Function\>](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function) | [\<string\>](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type) Filter function or path relative to the server root.
* `functionOrPath` [\<Function\>](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function) | [\<string\>](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type) Routing function or path to a static file.
* Returns: [\<Server\>](#server) A new server that will route requests to the specified function.

Routes requests to the specified function.
When using a string as `filter`, requests with a `url` property that matches `filter` will be routed.
When using a string as `functionOrPath`, [margay.static(filePath[, statusCode[, mimeType]])](#margay-static) is implicitly called with a `statusCode` of `200`.

#### <a name="server-use"></a>server.use(middleware)
* `middleware` [\<Function\>](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function)
* Returns: [\<Server\>](#server) A new server that will use the specified middleware.

Registers middleware that can manipulate data sent by the server.

## Examples
### Static file server
```js
const margay = require("margay");
const server = margay
  .createServer()
  .route("/", "./html/index.html")
  .route("/about", "./html/about.html")
  .default("./html/404.html");

server.listen();
```

### API server
```js
const margay = require("margay");
const server = margay
  .createServer()
  .route("/api/users", (request, response) => {
    if (request.method === "POST") {
      // Do database operations etc.
      response
        .status(200) // "OK"
        .header("Content-Type": "application/json")
        .end(JSON.stringify({status: "success"}));
    } else {
      response
        .status(405) // "Method Not Allowed"
        .end();
    }
  })
  .default((request, response) => {
    response
      .status(404) // "Not Found"
      .end();
  });

server.listen();
```

## License

[ISC](./LICENSE.md)