const http = require("http");
const Response = require("./Response.js");
const {static} = require("./utils.js");




function Server(options = {}) {
  const {middleware, routes} = Object.assign({
    middleware: [],
    routes: {
      default: (request, response) => {
        response
          .status(404)
          .header("Content-Type", "text/plain")
          .end("404");
      },
      path: []
    }
  }, options);
  return Object.create(prototype, {
    middleware: {
      enumerable: true,
      value: middleware
    },
    routes: {
      enumerable: true,
      value: {
        default: routes.default,
        path: routes.path
      }
    }
  });
}




const prototype = {
  default(functionOrPath) {
    const routeFunction = typeof functionOrPath === "string" ? static(functionOrPath, 404) : functionOrPath;

    return Server({middleware: this.middleware, routes: {default: routeFunction, path: this.routes.path}});
  },


  listen(port = 80) {
    http.createServer(async (request, response) => {

      const responseProxy = Response(request, response, this.middleware);
      const isRouted = this.routes.path.reduce(async (isRouted, route) => {
        if (await isRouted) return true;
        if (route.filter(request)) {
          await route.fn(request, responseProxy);
          return true;
        } else {
          return false;
        }
      }, false);

      await isRouted
        ? undefined
        : this.routes.default(request, responseProxy);

    }).listen(port);
    return this;
  },


  route(filter, functionOrPath) {
    const filterFunction = typeof filter === "string" ? ({url}) => url === filter : filter;
    const routeFunction = typeof functionOrPath === "string" ? static(functionOrPath, 200) : functionOrPath;

    return Server({middleware: this.middleware, routes: {default: this.routes.default, path: [...this.routes.path, {filter: filterFunction, fn: routeFunction}]}});
  },


  use(fn) {
    return Server({middleware: [...this.middleware, fn], routes: this.routes});
  }
};




module.exports = Server;