const fs = require("fs");
const Mime = require("mime/Mime");




const static = (path, code = 200, type = (new Mime()).getType(path)) => (request, response) => {
  fs.readFile(path, (err, buf) => {
    if (err) throw err;
    response
      .status(code)
      .header("Content-Type", type)
      .end(buf);
  });
}




module.exports = {static};