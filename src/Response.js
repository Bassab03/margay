function Response(request, response, middleware) {
  return Object.create(prototype, {
    middleware: {
      value: middleware
    },
    response: {
      value: response
    },
    request: {
      value: request
    }
  });
}




const prototype = {
  end(chunk) {
    if (typeof chunk !== "undefined") this.write(chunk);
    this.response.end();
  },

  header(name, value) {
    this.response.setHeader(name, value);
    return this;
  },

  status(code) {
    this.response.statusCode = code;
    return this;
  },

  write(chunk) {
    const accumulator = this.middleware.reduce((accumulator, middleware) => middleware(accumulator, this.request, this.response), chunk.toString());
    this.response.write(accumulator);
    return this;
  }
};




module.exports = Response;